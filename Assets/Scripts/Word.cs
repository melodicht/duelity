using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Word : MonoBehaviour
{
    public bool isTrigger;

    public float fallingSpeed;

    private float exitY = -560f;

    private WordsManager wordsManager;

    void Start() 
    {
        wordsManager = GameObject.FindGameObjectWithTag("WordsManager").GetComponent<WordsManager>();
    }

    public void InitializeWord(string word)
    {
        GetComponent<Text>().text = word;
    }

    void FixedUpdate()
    {
        transform.position = new Vector3(
            transform.position.x, 
            transform.position.y - (fallingSpeed * Time.fixedDeltaTime * 10f), 
            transform.position.z);

        if (transform.position.y <= exitY)
        {
            if (isTrigger) wordsManager.Death();
            Destroy(gameObject);
        }
    }


}