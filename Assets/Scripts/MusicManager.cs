using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using UnityEngine;
using UnityEngine.Audio;

public class MusicManager : MonoBehaviour
{
    public static AudioSource[] aud = new AudioSource[2];

    private bool activeSource;  // Bool to keep track of two states
    private IEnumerator musicTransition;

    public enum MusicType { SILENCE, DRUNK, FINALE, DUEL, DIALOGUE };
    [System.Serializable]
    private struct Music
    {
        public MusicType type;
        public AudioClip clip;

    }

    [SerializeField]
    private List<Music> musicList;

    [SerializeField]
    private float crossFadeDuration;

    [Range(0f, 1f)]
    public float volume;

    public MusicType startingMusic;

    void Awake()
    {
        aud[0] = gameObject.AddComponent<AudioSource>();
        aud[0].loop = true;
        aud[1] = gameObject.AddComponent<AudioSource>();
        aud[1].loop = true;

        // AudioMixer audioMixer = Resources.Load<AudioMixer>("MainMixer");
        // AudioMixerGroup[] audioMixGroup = audioMixer.FindMatchingGroups("Music");

        // aud[0].outputAudioMixerGroup = audioMixGroup[0];
        // aud[1].outputAudioMixerGroup = audioMixGroup[0];

        Switch(startingMusic);
    }

    public void Switch(MusicType type, int transitionDuration = 10)
    {
        AudioClip clip = GetClip(type);

        int nextSource = !activeSource ? 0 : 1;
        int currentSource = activeSource ? 0 : 1;

        if (clip == aud[currentSource].clip) return;
 
        // If a transition is already happening, stop old one
        if (musicTransition != null) StopCoroutine(musicTransition);
 
        aud[nextSource].clip = clip;
        aud[nextSource].Play();
 
        musicTransition = Transition(transitionDuration);
        StartCoroutine(musicTransition);
    }

    IEnumerator Transition(int transitionDuration)
    {
        // One transition duration is a tenth of a second.
        for (int i = 0; i < transitionDuration + 1; i++) {
            aud[0].volume = activeSource ? (transitionDuration - i) * (1f / transitionDuration) : (0 + i) * (1f / transitionDuration);
            aud[1].volume = !activeSource ? (transitionDuration - i) * (1f / transitionDuration) : (0 + i) * (1f / transitionDuration);
 
            aud[0].volume *= volume;
            aud[1].volume *= volume;
 
            // Real time to circumvent game pause
            yield return new WaitForSecondsRealtime(0.1f);
        }
 
        aud[activeSource ? 0 : 1].Stop();
 
        activeSource = !activeSource;
        musicTransition = null;
    }

    private AudioClip GetClip(MusicType type)
    {
        return musicList.Find(music => music.type == type).clip;
    }
}
