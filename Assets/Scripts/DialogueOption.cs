using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueOption : MonoBehaviour
{
	[HideInInspector] public string head;
	[HideInInspector] public string connorMessage;

	[SerializeField] private DialogueManager dialogueManager;

	void Start()
	{
		dialogueManager = GameObject.FindGameObjectWithTag("DialogueGO").GetComponent<DialogueManager>();
	}

	public void activateNextMessage()
	{
		dialogueManager.LoadDialogue(head);
		dialogueManager.ShowConnorMessage(connorMessage);
	}

	public void NextLevel()
	{
		GameManager.instance.NextLevel();
	}

	public void Duel()
	{
		GameManager.instance.Duel();
	}
}