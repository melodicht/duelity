using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
	private IDictionary<string, string> headToContent = new Dictionary<string, string>();

	private bool isResponding = false;

	public string otherName;
	private string otherMessage;

	[SerializeField] private Text nameText;
	[SerializeField] private Text messageText;

	[SerializeField] private GameObject messagePanel;
	[SerializeField] private GameObject optionsGO;
	[SerializeField] private GameObject optionPrefab;
	[SerializeField] private GameObject optionPersuadePrefab;
	[SerializeField] private GameObject optionDuelPrefab;

	private int level;

	void Update()
	{
		if ((Input.GetKeyDown("space") || Input.GetMouseButtonDown(0)) && !isResponding)
		{
			messagePanel.SetActive(false);
			optionsGO.SetActive(true);
		}
		
		if ((Input.GetKeyDown("space") || Input.GetMouseButtonDown(0)) && isResponding)
		{
			nameText.text = otherName;
			messageText.text = otherMessage;
			isResponding = false;
		}
	}

	public void Initialize(List<string[]> rawDialogueNodes, string name, int level)
	{
		this.level = level;
		otherName = rawDialogueNodes[0][0].Trim();
		PopulateHeadToContent(rawDialogueNodes);
		LoadDialogue(otherName);
		otherName = name;
		messagePanel.SetActive(true);
		optionsGO.SetActive(false);
		nameText.text = otherName;
		messageText.text = otherMessage;

		isResponding = false;
	}

	void PopulateHeadToContent(List<string[]> rawDialogueNodes)
	{
		foreach(string[] rawDialogueNode in rawDialogueNodes)
		{
			headToContent[rawDialogueNode[0]] = rawDialogueNode[1];
		}
	}

	public void LoadDialogue(string head)
	{
		if (head == null) Debug.Log("Error! Load Dialogue given null.");

		if (level == 1)
		{
			AudioManager.instance.Play("Gulp");
		}
		else if (level == 2)
		{
			AudioManager.instance.Play("Smug Hmm");
		}

		string content = headToContent[head];

		otherMessage = content.Substring(0, content.IndexOf("[")).Trim();

		string responseString = content.Substring(content.IndexOf("["));

		string[] responses = responseString.Split(new string[] { "[[", "]]" }, StringSplitOptions.RemoveEmptyEntries);
		responses = responses.Where(x => !string.IsNullOrEmpty(x.Trim().Trim('\"'))).ToArray();

		foreach (Transform child in optionsGO.transform) {
			GameObject.Destroy(child.gameObject);
		}	

		foreach (string response in responses)
		{
			string[] textAndHead = response.Split(new string[] { "->" }, StringSplitOptions.RemoveEmptyEntries);

			string responseText = textAndHead[0];

			if (responseText[0] == '(')
			{
				string buttonText = responseText.Substring(1, responseText.IndexOf(")") - 1);
				string currConnorMessage = responseText.Substring(responseText.IndexOf(")") + 1);

				string buttonHeadLink = textAndHead[1];

				GameObject newOption = Instantiate(optionPrefab, optionsGO.transform);

				newOption.GetComponent<DialogueOption>().head = buttonHeadLink;
				newOption.GetComponent<DialogueOption>().connorMessage = currConnorMessage;
				newOption.GetComponentInChildren<Text>().text = buttonText;
			}
			else
			{
				if (responseText.Contains("Persuaded")) 
				{
					GameObject newOption = Instantiate(optionPersuadePrefab, optionsGO.transform);
					newOption.GetComponentInChildren<Text>().text = responseText;
				}
				else if (responseText.Contains("Duel")) 
				{
					GameObject newOption = Instantiate(optionDuelPrefab, optionsGO.transform);
					newOption.GetComponentInChildren<Text>().text = responseText;
				}
				else
				{
					Debug.Log("Response text is neither persuaded nor duel...");
				}
			}
			
		}
		
	}

	public void ShowConnorMessage(string message)
	{
		isResponding = true;
		messagePanel.SetActive(true);
		optionsGO.SetActive(false);
		nameText.text = "Connor";
		messageText.text = message.Trim();
	}
}