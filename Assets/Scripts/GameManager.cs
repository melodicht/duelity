using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public static GameManager instance;

	private int level = 0;

	private IDictionary<int, List<string[]>> idToRawDialogueNodes;

	[SerializeField] private string dialogueFile;

	[SerializeField] private DialogueManager dialogueManager;
	[SerializeField] private WordsManager wordsManager;

	[SerializeField] private GameObject dialogueGO;
	[SerializeField] private GameObject duelGO;

	[SerializeField] private GameObject dialogueBG;
	[SerializeField] private GameObject characterSprite;

	[SerializeField] private Animator animator;

	[SerializeField] private GameObject endScreenGO;

	[SerializeField] private List<Level> levels;

	void Awake()
	{
		if (instance == null) instance = this;
		else if (instance != this) Destroy(gameObject);

		PopulateIdToRawDialogueNodes();
	}

	public void PopulateIdToRawDialogueNodes()
	{
		idToRawDialogueNodes = new Dictionary<int, List<string[]>>();

		TextAsset textAsset = Resources.Load<TextAsset>(dialogueFile);
		string[] rows = textAsset.text.Split(new string[] { "####" }, StringSplitOptions.RemoveEmptyEntries);

		foreach (string row in rows) {
			string[] rowCells = row.Split(',');
			rowCells[1] = rowCells[1].Replace('`', ',');
			if (rowCells.Length >= 3) 
				rowCells[2] = rowCells[2].Replace('`', ',');

			// Split id from rowCells
			int id = int.Parse(rowCells[0]);
			rowCells = rowCells.Skip(1).ToArray();

			if (idToRawDialogueNodes.ContainsKey(id))
			{
				idToRawDialogueNodes[id].Add(rowCells);
			}
			else
			{
				idToRawDialogueNodes.Add(id, new List<string[]>{ rowCells });
			}
		}
	}

	public void StartGame() {
		BeginDialogue(level);
	}

	public void NextLevel()
	{
		level++;
		if (level >= 5)
		{
			endScreenGO.SetActive(true);
		}
		else
		{
			BeginDialogue(level);
		}
	}

	public void RestartLevel()
	{
		BeginDialogue(level);
	}

	private void BeginDialogue(int level)
	{
		duelGO.SetActive(false);
		dialogueGO.SetActive(true);
		dialogueManager.Initialize(idToRawDialogueNodes[level], levels[level].name, level);
		dialogueBG.GetComponent<SpriteRenderer>().sprite = levels[level].backgroundSprite;
		characterSprite.GetComponent<Image>().sprite = levels[level].personSprite;

		if (level == 1)
		{
			AudioManager.instance.musicManager.Switch(MusicManager.MusicType.DRUNK);
		}
		else if (level == 4)
		{
			AudioManager.instance.musicManager.Switch(MusicManager.MusicType.FINALE);
		}
		else
		{
			AudioManager.instance.musicManager.Switch(MusicManager.MusicType.DIALOGUE);
		}
	}

	public void Duel()
	{
		animator.runtimeAnimatorController = levels[level].animatorController;
		duelGO.SetActive(true);
		dialogueGO.SetActive(false);
		wordsManager.triggerWord = levels[level].triggerPhrase;
		wordsManager.SetWords(levels[level].promptFileName);
		wordsManager.activated = true;
		wordsManager.death = false;

		if (level == 4)
		{
			AudioManager.instance.musicManager.Switch(MusicManager.MusicType.FINALE);
		}
		else
		{
			AudioManager.instance.musicManager.Switch(MusicManager.MusicType.DUEL);
		}
	}
}