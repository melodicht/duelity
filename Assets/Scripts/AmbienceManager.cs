using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AmbienceManager : MonoBehaviour
{
    [SerializeField]
    private List<AudioClip> sfxList;

    private AudioSource chatterSource;
    private AudioSource sfxSource;

    private float timeUntilRandomEffect;

    void Start()
    {
        // chatterSource = gameObject.AddComponent<AudioSource>();
        sfxSource = gameObject.AddComponent<AudioSource>();

        // Linking sources with the audio mixer group
        // AudioMixer audioMixer = Resources.Load<AudioMixer>("MainMixer");
        // AudioMixerGroup[] audioMixGroup = audioMixer.FindMatchingGroups("Ambience");
        // chatterSource.outputAudioMixerGroup = audioMixGroup[0];
        // sfxSource.outputAudioMixerGroup = audioMixGroup[0];

        // Initialize random sfx
        timeUntilRandomEffect = UnityEngine.Random.Range(5, 30);
    }

    void Update()
    {
        if (timeUntilRandomEffect <= 0)
        {
            PlayRandomSfx();
            timeUntilRandomEffect = UnityEngine.Random.Range(5, 30);
        }
        timeUntilRandomEffect -= Time.deltaTime;
    }

    private void PlayRandomSfx()
    {
        sfxSource.clip = sfxList[UnityEngine.Random.Range(0, sfxList.Count)];
        sfxSource.loop = false;
        sfxSource.Play();
    }
}
