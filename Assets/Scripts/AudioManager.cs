using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public MusicManager musicManager;
    public AmbienceManager ambienceManager;
    public Sound[] sounds;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyChildOnLoad(gameObject);

        foreach (Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;

            // AudioMixer audioMixer = Resources.Load<AudioMixer>("MainMixer");
            // AudioMixerGroup[] audioMixGroup = audioMixer
            //     .FindMatchingGroups("Sfx");
            // sound.source.outputAudioMixerGroup = audioMixGroup[0];
        }
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning($"Sound: \"{ name }\" not found.");
            return;
        }
        s.source.Play();
    }

    private static void DontDestroyChildOnLoad(GameObject child)
     {
        Transform parentTransform = child.transform;

        while (parentTransform.parent != null)
        {
            parentTransform = parentTransform.parent;
        }
        GameObject.DontDestroyOnLoad(parentTransform.gameObject);
     }
}
