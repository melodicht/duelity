using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// To initialize
// - set trigger word
// - set min and max pos x
// - set this.words

public class WordsManager : MonoBehaviour
{
    public string triggerWord;

    [SerializeField] private Camera cam;

    private bool sentTriggerWord = false;
    private float time;
    private string[] words;

    [SerializeField] private int wordsPerSec;
    [SerializeField] private float minPosX;
    [SerializeField] private float maxPosX;
    [SerializeField] private float minFallSpeed;
    [SerializeField] private float maxFallSpeed;

    [SerializeField] private GameObject wordPrefab;

    private GameObject canvas;

    public bool activated;

    [SerializeField] private GameObject gameOverGO;
    [SerializeField] private GameObject winScreenGO;

    public bool death = false;


    void Start() 
    {
        canvas = GameObject.FindGameObjectWithTag("DuelingCanvas");

        time = 1f / wordsPerSec;;

        activated = true;
    }

    public void SetWords(string wordsFile)
    {
        TextAsset textAsset = Resources.Load<TextAsset>(wordsFile);
        char[] separators = new char[] { '.' };

        words = textAsset.text.Split(separators, StringSplitOptions.RemoveEmptyEntries);

        foreach (string word in words)
        {
            word.Trim();
        }
    }

    public void Death()
    {
        activated = false;
        ClearDuelingCanvas();
        gameOverGO.SetActive(true);
        death = true;
    }

    void ClearDuelingCanvas()
    {
        foreach (Transform child in canvas.transform) {
			GameObject.Destroy(child.gameObject);
		}	
    }

    void FixedUpdate()
    {
        if (death) return;
        if (Input.GetKeyDown("space") || Input.GetMouseButtonDown(0))
        {
            if (sentTriggerWord)
            {
                AudioManager.instance.Play("Pistol Shot");
                activated = false;
                ClearDuelingCanvas();
                winScreenGO.SetActive(true);
            }
            else
            {
                Death();
            }
        }

        if (!activated) return;

        if (time <= 0)
        {
            SpawnNext();
            time = 1f / wordsPerSec;
        }
        
        time -= Time.fixedDeltaTime;
    }

    void SpawnNext()
    {
        int index = UnityEngine.Random.Range(0, words.Length - 1);
        string toSpawn = words[index].Trim();

        if (String.Equals(toSpawn, triggerWord.Trim())) sentTriggerWord = true;

        GameObject newWord = Instantiate(wordPrefab, new Vector3(UnityEngine.Random.Range(minPosX, maxPosX) + 960f, 1200f, 0f), Quaternion.identity, canvas.transform);

        newWord.GetComponent<Word>().InitializeWord(toSpawn);
        newWord.GetComponent<Word>().fallingSpeed = UnityEngine.Random.Range(minFallSpeed, maxFallSpeed);
        newWord.GetComponent<Word>().isTrigger = String.Equals(toSpawn, triggerWord);
    }
}