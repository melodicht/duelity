using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
// using UnityEditor.Animations;

[System.Serializable]
public class Level
{
  public string name;
  public Sprite backgroundSprite;
  public Sprite personSprite;
  public string triggerPhrase;
  public string promptFileName;
  public AnimatorOverrideController animatorController;
}